.vimrc:
https://github.com/mscoutermarsh/dotfiles
https://github.com/thoughtbot/dotfiles

Remap Caps Lock to ESC

Speed up your Key Repeat: Mac: Karabiner

$vimtutor
vim-adventures.com

http://vimcasts.org
https://upcase.com/vim

Plugins:
Ctrl-P (Fuzzy File Search)
NERDTree
AG for VIM (Search through Projects)
